# README #

De momento este README será utilizado como referencia, contendrá fuentes y ejemplos para poder comenzar el desarrollo del diario en linea.

### Como usar el repo? ###
Lo ideal es subir las nuevas lineas de código cuando se tenga algún cambio importante o una función esté completa; NUNCA suban algo si tiene errores o no esta funcionando.
Para usar GIT 

1. Sincronizar con mi repositorio los últimos cambios. 
2. Luego de sincronizar; en su equipo, EN LA CARPETA DEL PROYECTO escribir **git checkout**
3. **git pull**
4. Luego cuando hagan algún cambio, con una función completa realizar 3 pasos.
5. **git add -A**
6. **git commit -a -m "Mensaje que informe del cambio"**
7. **git push**
8. Luego en el sitio, realizan un **pull request** desde su repositorio hacia el mio, respetando la misma rama (trabajaremos con la rama master siempre).


### ¿Herramientas que estamos usando? ###

* MongoDB [(Query a documentos)](http://docs.mongodb.org/manual/tutorial/query-documents/)
* PyMongo [Tutorial uso basico, conexion y query simple](http://api.mongodb.org/python/current/tutorial.html)
* Bottle.py [Tutorial uso general, POST,GET,template,etc](http://bottlepy.org/docs/dev/tutorial.html)

#Por ultimo; Traten de ser proactivos y autodidactas#