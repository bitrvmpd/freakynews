__author__ = 'e'

from bottle import route, run,template,static_file,get, post, request,response,redirect
import pymongo
import time


#Prueba para determinar si puedo almacenar la conexion a nivel de clase
connection = None

def getCollection(coleccion):
     try:
        dbname = 'teaSoft'
        #connection = pymongo.MongoClient(os.environ['OPENSHIFT_MONGODB_DB_URL'])
        global connection
        connection = pymongo.MongoClient('localhost', 27017)
        db = connection[dbname]
        if(coleccion=='freakyNews'):
            collection = db.freakyNews
        elif(coleccion=='comentarios'):
            collection = db.comentarios
        elif(coleccion=='autores'):
            collection = db.autores

        return collection
     except:
         return "algo salio mal"

@route('/loc')
def cargaDB():

    try:
        #dbname = 'teaSoft'
        #connection = pymongo.MongoClient(os.environ['OPENSHIFT_MONGODB_DB_URL'])
        #connection = pymongo.MongoClient('localhost', 27017)
        #db = connection[dbname]
        collection = getCollection("freakyNews")
    except:
         return ("Couldn't connect to database")

    #################################
    try:
        #conocido tambien como el menu de la aplicacion web
        salida_html = ""
        lista_canales = collection.find()
        for canal in lista_canales:
            #print canal
            salida_html = salida_html + "<p>" + canal['cuerpo'] + "</p><br/>"
        global connection #de esta forma llamo a variables globales, una lata.
        connection.close()
        return template('test',{'variableDesdePython' : salida_html})

    except:
        return ("Prasdasdasdata to template")

@route('/categorias/copuchas')
def getCopuchas():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':'Copuchas'})

    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
          <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">"""+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                        """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close

    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})


@route('/categorias/mixta/cronicas')
def getMixta_cronica():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Mixta','Cronica']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">"""+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                           """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close

    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})

@route('/categorias/mixta/reportajes')
def getMixta_reportaje():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Mixta','Reportaje']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">"""+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                           """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close

    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})

##################################################################
@route('/categorias/informativa/noticias')
def getInformativa():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Informativa','Noticia']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">"""+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                             """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                              """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close

    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})

@route('/categorias/informativa/entrevistas')
def getInformativa_entevistas():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Informativa','Entrevista']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">"""+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                             """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                              """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close

    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})


@route('/categorias/informativa/resenas')
def getInformativa_resena():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Informativa','Resena']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">"""+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                                """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                              """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close

    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})


@route('/categorias/informativa/reportajes')
def getInformativa_reportaje():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Informativa','Reportaje']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <h1 class="brand-heading">
                        """+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                              """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>

                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close
    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})


@route('/categorias/entretencion/musica')
def getInformativa_reportaje():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Entretencion','Musica']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <h1 class="brand-heading">
                        """+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                              """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>

                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close
    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})

@route('/categorias/entretencion/eventos')
def getInformativa_reportaje():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Entretencion','Eventos']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <h1 class="brand-heading">
                        """+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                              """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>

                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close
    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})

@route('/categorias/entretencion/online')
def getInformativa_reportaje():
    indice = 0
    coleccion = getCollection("freakyNews")
    salidahtml = ""
    lista_noticias = coleccion.find({'categoria':['Entretencion','Online']})
    for noticia in lista_noticias:
        #comienzo a armar la pagina de la noticia
        salidahtml = salidahtml + """
           <section  id="titulo"""+str(indice)+"""" class="intro" style="background:  -webkit-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* For Safari 5.1 to 6.0 */
    background: -o-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Opera 11.1 to 12.0 */
    background:   -moz-linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll;  /* For Firefox 3.6 to 15 */
    background:   linear-gradient(transparent, black), url("""+noticia['imagen']+""")  no-repeat bottom center scroll; /* Standard syntax (must be last) */;
    background-color: #000;
    -webkit-background-size: cover;
    -moz-background-size: cover;
    background-size: cover;
    -o-background-size: cover;
    ">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">

                        <h1 class="brand-heading">
                        """+noticia['titulo']+"""</h1>
                        <p class="intro-text">"""+noticia['subtitulo']+"""</p>
                        <div class="page-scroll">
                            <a href="#cuerpo"""+str(indice)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                            """
        if(request.get_cookie("usuario")!=None):
            test = 'value="'+ noticia['titulo'] +'"'
            salidahtml = salidahtml + """
                            <form  id="titulo"""+str(indice)+"""" name="titulo"""+str(indice)+""""  action="/eliminar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice)+""");" ><i class="fa fa-minus-circle animated"></i></a>
                        </form>

                        <form  id="titulo"""+str(indice+1)+"""" name="titulo"""+str(indice+1)+""""  action="/editar" method="post">
                        <input id="titulo" name="titulo" type="hidden" """+test+""">
                        <a class="btn btn-circle" onclick="formulario("""+str(indice+1)+""");"  ><i class="fa fa-pencil-square-o animated"></i> </a>
                        </form>
                        """
        salidahtml = salidahtml + """
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="cuerpo"""+str(indice)+"""" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
               <p>"""+noticia['cuerpo']+"""</p>
                <div class="page-scroll">
                              """
        if(indice+1< lista_noticias.count()):
            salidahtml = salidahtml + """
                            <a href="#titulo"""+str(indice+1)+"""" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>

                            """
        salidahtml = salidahtml + """
                        </div>
            </div>
        </div>
    </section>
        """
        indice = indice+1

    global connection
    connection.close
    if(request.get_cookie("usuario")!=None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/do_logout','texto':'Cerrar Sesion','agregar':salida})
    else:
        return template('noticias',{'cuerpoNoticias':salidahtml,'accion':'/login','texto':'Iniciar Sesion','agregar':''})

################################

@post('/do_nuevaPublicacion') # or @route('/login', method='POST')
def do_nuevaPublicacion():
    try:

        titulo = request.forms.get('titulo')
        subtitulo = request.forms.get('subtitulo')
        cuerpo = request.forms.get('cuerpo')
        categoria = request.forms.get('categoria')
        categoria = categoria.split(',');
        #tags separados por coma
        tags = request.forms.get('tags')
        tags = tags.split(',');
        imagen = request.forms.get('imagen')

        coleccion = getCollection("freakyNews")
        datos = {
            'titulo':titulo,
            'subtitulo': subtitulo,
            'cuerpo':cuerpo,
            'categoria':categoria,
            'tags':tags,
            'imagen':imagen,
            'fecha':time.strftime("%d/%m/%Y"),
            'autor':request.get_cookie("usuario")
        }
        coleccion.insert(datos)

    except:
        print "error"
    finally:
        global connection
        connection.close

        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('exito',{'exito':'La publicacion fue almacenada correctamente','agregar':salida,'accion':'/do_logout','texto':'Cerrar Sesion'})



@post('/do_nuevoComentario') # or @route('/login', method='POST')
def do_nuevoComentario():
    try:

        nombre = request.forms.get('nombre')
        cuerpo = request.forms.get('cuerpo')
        categoria = request.forms.get('categoria')
        coleccion = getCollection("comentarios")
        datos = {
            'nombre' : nombre,
            'cuerpo':cuerpo,
            'categoria':categoria,
            'fecha':time.strftime("%d/%m/%Y")
        }
        coleccion.insert(datos)

    except:
        print "error"
    finally:
        global connection
        connection.close
        if(request.get_cookie("usuario")!=None):
            salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
            return template('exito',{'exito':'El formulario fue enviado correctamente','agregar':salida,'accion':'/do_logout','texto':'Cerrar Sesion'})
        else:
            return template('exito',{'exito':'El formulario fue enviado correctamente','agregar':"",'accion':'/do_login','texto':'Iniciar Sesion'})



@post('/do_login')
def do_login():
    try:
        usuario = request.forms.get('usuario')
        contrasena = request.forms.get('contrasena')
        coleccion = getCollection("autores")
        credenciales = coleccion.find({"usuario":usuario,"contrasena":contrasena}).count()

        if(credenciales==1):
            response.set_cookie("usuario",usuario,secret=None,max_age=3600*24*12)
            redirect('/')
        else:
            response.delete_cookie("usuario")
            redirect('/error')
    finally:
        global connection
        connection.close

@post('/eliminar')
def do_eliminar():
    try:
        titulo = request.forms.get('titulo')
        coleccion = getCollection("freakyNews")
        coleccion.remove({"titulo":titulo})

        salida = """<li class="page-scroll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('exito',{'exito':'El formulario fue enviado correctamente','agregar':salida,'accion':'/do_logout','texto':'Cerrar Sesion'})

    finally:
        global connection
        connection.close


@post('/editar')
def do_editar():
    try:
        titulo = request.forms.get('titulo')
        coleccion = getCollection("freakyNews")
        stringTag = ""

        datos = coleccion.find_one({"titulo":titulo})
        stringTag = datos['tags']

        return template('editarPublicacion',{'titulo':datos['titulo'],'subtitulo':datos['subtitulo'],'cuerpo':datos['cuerpo'],'imagen':datos['imagen'],'tags':stringTag})

    finally:
        global connection
        connection.close

@post('/do_updatePublicacion')
def do_updatepub():
    try:
        titulo = request.forms.get('titulo')
        subtitulo = request.forms.get('subtitulo')
        cuerpo = request.forms.get('cuerpo')
        imagen = request.forms.get('imagen')
        tags = request.forms.get('tags')

        coleccion = getCollection("freakyNews")
        coleccion.update({'titulo': titulo}, {'$set': {'subtitulo': subtitulo,'cuerpo':cuerpo,'imagen':imagen,'tags':tags}})
        salida = """<li class="page-scroll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('exito',{'exito':'El formulario fue enviado correctamente','agregar':salida,'accion':'/do_logout','texto':'Cerrar Sesion'})
    finally:
        global connection
        connection.close

@route('/do_logout')
def do_logout():
    try:
        response.delete_cookie("usuario")
    except:
        print "error"

    finally:
        redirect('/')

@route('/login')
def login():
    return template('login')
@route('/error')
def error():
    return template('error',{"error":"No pudo iniciar sesion"})

@route('/nuevaPublicacion')
def nuevaPublicacion():
    if(request.get_cookie("usuario")!=None):
        return template('nuevaPublicacion')
    else:
        return template('error',{'error':'No tiene los privilegios para acceder a esta pagina'})

@route('/')
def index():
    usuario = request.get_cookie("usuario")
    if(usuario != None):
        salida = """<li class="page-scrooll">
                        <a href="/nuevaPublicacion"><i class="fa fa-plus"></i>Nueva Publicacion</a>
                    </li>"""
        return template('index',{"accion":"/do_logout","texto":"Cerrar Sesion","agregar":salida})
    else:
        salida=""
        return template('index',{"accion":"/login","texto":"Iniciar Sesion","agregar":salida})

@route('/static/css/<filename:re:.*\.css>')
def send_static(filename):
    return static_file(filename, root='static/css/')

@route('/static/js/<filename:re:.*\.js>')
def send_static(filename):
    return static_file(filename, root='static/js/')

@route('/static/img/<filename:re:.*\.(jpg|png|gif|ico)>')
def send_static(filename):
    return static_file(filename, root='static/img/')




run(host='0.0.0.0', port=5632, debug=True)