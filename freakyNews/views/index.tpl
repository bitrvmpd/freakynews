<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Free One Page Theme for Bootstrap 3</title>

    <!-- Bootstrap Core CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
   <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Custom Theme CSS -->
    <link href="/static/css/grayscale.css" rel="stylesheet">

    <script type="text/javascript" src="/static/js/jquery-1.10.2.js">
    </script>
    <style>
    .formulario{
        font-size:25px;background:black;color:white;border style:solid;border-color:white;
    }
    </style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">
                    <i class="fa fa-leaf"></i> <span class="light">teaSoft</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about">Acerca de</a>
                    </li>
                    <li class="page-scroll">
                        <a class="dropdown-toggle"  data-toggle="dropdown" >Categorias</a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                            <a id="informativa" href="categorias/informativa/noticias">Informativa/Noticias</a>

                            </li>
                            <li>
                            <a id="informativa" href="categorias/informativa/entrevistas">Informativa/Entrevistas</a>

                            </li>
                            <li>
                            <a id="informativa" href="categorias/informativa/resenas">Informativa/Reseñas</a>

                            </li>
                            <li>
                            <a id="informativa" href="categorias/informativa/reportajes">Informativa/Reportajes</a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="mixta" href="categorias/mixta/cronicas">Mixta/Cronicas</a>
                            </li>
                             <li>
                                <a id="mixta" href="categorias/mixta/reportajes">Mixta/Reportajes</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="copuchas" href="categorias/copuchas">Copuchas</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="entretencion" href="categorias/entretencion/musica">Entetencion/Musica</a>
                            </li>
                            <li>
                                <a id="entretencion" href="categorias/entretencion/eventos">Entetencion/Eventos</a>
                            </li>
                            <li>
                                <a id="entretencion" href="categorias/entretencion/online">Entetencion/Juegos en Línea</a>
                            </li>

                        </ul>
                    </li>
                    <li class="page-scroll">
                        <a href="#contact">Contacto</a>
                    </li>
                    {{!agregar}}
                    <li class="page-scroll">
                        <a href="{{accion}}"><i class="fa fa-child"></i>{{texto}}</a>
                    </li>
                   </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <section class="intro2">
        <div class="intro-body">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <h1 class="brand-heading">freaky News</h1>
                        <p class="intro-text">Tu source principal de noticias.</p>
                        <div class="page-scroll">
                            <a href="#about" class="btn btn-circle">
                                <i class="fa fa-angle-double-down animated"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Acerca de freakyNews</h2>
                <h5>(un producto de teaSoft)</h5>
                <p>Agradecemos a <a href="https://twitter.com/bitrvmpd">Bitrvmpd[GS] </a> por darnos un subdominio para funcionar.</p>
            </div>
        </div>
    </section>

    <section id="download" class="content-section text-center">
        <div class="download-section">
            <div class="container">
                <div class="col-lg-8 col-lg-offset-2">
                    <h2>Nuestra visión</h2>
                    <p>En un mundo donde la tecnología esta inmersa en nuestras vidas, creemos que es necesario la creacion de un noticiero casual con los datos mas Freaks que puedes encontrar en internet.</p>
                </div>
            </div>
        </div>
    </section>

    <section  id="contact" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Contacta a freakyNews<h2>
                <form action="/do_nuevoComentario" method="post" name="formularioEnviar" id="formularioEnviar">
                <p>Envíanos tu opinion o comentario y aparecerás en <a href="#">este link</a></p>
                <input type="text" class="formulario"  placeholder="Nombre" id="nombre" name="nombre">
                <select class="formulario" id="categoria" name="categoria" >
                    <option  class="formulario" value="comentario">Comentario</option>
                    <option  class="formulario" value="opinion">Opinion</option>
                    <option  class="formulario" value="carta">Carta al director</option>
                </select> <br/>
                <textarea  style="resize: none;" class="formulario" placeholder="Ingrese su comentario u opinión" id="cuerpo" name="cuerpo" rows="5" cols="30"></textarea></br>
                <a onclick="javacript:submitform();" class="btn btn-default btn-lg"><i class="fa fa-paper-plane fa-fw"></i><span class="network-name">&nbsp&nbspEnviar</span></a>
                </form>
                <br/>
                <p>Tambien no dudes en enviarnos un correo con tu opinion sobre el sitio, o incluso si quieres decir ¡hola!</p>
                <a class="p" href="mailto:freakynews@bitrvmpd.com">freakynews@bitrvmpd.com</a>
                <br/>
                <br/>
                <br/>
                <br/>
                 <br/>
                <br/>
                <br/>
                <!--<ul class="list-inline banner-social-buttons">
                    <li><a href="https://twitter.com/SBootstrap" class="btn btn-default btn-lg"><i class="fa fa-twitter fa-fw"></i> <span class="network-name">Twitter</span></a>
                    </li>
                    <li><a href="https://github.com/IronSummitMedia/startbootstrap" class="btn btn-default btn-lg"><i class="fa fa-github fa-fw"></i> <span class="network-name">Github</span></a>
                    </li>
                    <li><a href="https://plus.google.com/+Startbootstrap/posts" class="btn btn-default btn-lg"><i class="fa fa-google-plus fa-fw"></i> <span class="network-name">Google+</span></a>
                    </li>
                </ul>-->
            </div>
        </div>
    </section>



    <!-- Core JavaScript Files -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="/static/js/grayscale.js"></script>
    <script type="text/javascript">
    function submitform(){
    document.formularioEnviar.submit();
    }</script>
</body>

</html>
