<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Grayscale - Free One Page Theme for Bootstrap 3</title>

    <!-- Bootstrap Core CSS -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css" rel="stylesheet" type="text/css">

    <!-- Fonts -->
   <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <!-- Custom Theme CSS -->
    <link href="/static/css/grayscale.css" rel="stylesheet">

    <script type="text/javascript" src="/static/js/jquery-1.10.2.js">
    </script>
    <style>
    .formulario{
        font-size:25px;background:black;color:white;border style:solid;border-color:white;
    }
    </style>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">

    <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="/#">
                    <i class="fa fa-leaf"></i> <span class="light">teaSoft</span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="/#about">Acerca de</a>
                    </li>
                    <li class="page-scroll">
                        <a class="dropdown-toggle"  data-toggle="dropdown" >Categorias</a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                            <a id="informativa" href="categorias/informativa/noticias">Informativa/Noticias</a>

                            </li>
                            <li>
                            <a id="informativa" href="categorias/informativa/entrevistas">Informativa/Entrevistas</a>

                            </li>
                            <li>
                            <a id="informativa" href="categorias/informativa/resenas">Informativa/Reseñas</a>

                            </li>
                            <li>
                            <a id="informativa" href="categorias/informativa/reportajes">Informativa/Reportajes</a>

                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="mixta" href="categorias/mixta/cronicas">Mixta/Cronicas</a>
                            </li>
                             <li>
                                <a id="mixta" href="categorias/mixta/reportajes">Mixta/Reportajes</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="copuchas" href="categorias/copuchas">Copuchas</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="entretencion" href="categorias/entretencion/musica">Entetencion/Musica</a>
                            </li>
                            <li>
                                <a id="entretencion" href="categorias/entretencion/eventos">Entetencion/Eventos</a>
                            </li>
                            <li>
                                <a id="entretencion" href="categorias/entretencion/online">Entetencion/Juegos en Línea</a>
                            </li>
                        </ul>
                    </li>
                    <li class="page-scroll">
                        <a href="/#contact">Contacto</a>
                    </li>
                    <li class="page-scrooll">
                        <a href="#login"><i class="fa fa-child"></i>Iniciar Sesión</a>
                    </li>
                   </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


    <section  id="login" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>Iniciar Sesión<h2>
                <form action="/do_login" method="post" name="formularioEnviar" id="formularioEnviar">
                <input type="text" class="formulario"  placeholder="usuario" id="usuario" name="usuario"></br>
                <input type="password" class="formulario"  placeholder="contraseña" id="contrasena" name="contrasena"></br>
                <a onclick="javacript:submitform();" class="btn btn-default btn-lg"><i class="fa fa-paper-plane fa-fw"></i><span class="network-name">&nbsp&nbspEnviar</span></a>
                </form>
                <br/>
            </div>
        </div>
    </section>



    <!-- Core JavaScript Files -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>


    <!-- Custom Theme JavaScript -->
    <script src="/static/js/grayscale.js"></script>
    <script type="text/javascript">
    function submitform(){
    document.formularioEnviar.submit();
    }</script>
</body>

</html>
